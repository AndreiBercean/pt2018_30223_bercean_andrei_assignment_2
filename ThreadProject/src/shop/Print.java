package shop;

import javax.swing.JTextArea;

public class Print extends Thread
{	
	private JTextArea sim;
	private Scheduler output;
	private boolean function = true;
	
	public Print(JTextArea s, Scheduler out)
	{
		sim = s;
		output = out;
		function = true;
	}
	
	public void run()
	{
		while(function)
		{
			sim.setText(output.toString());
			output.info();
		}
	}
	
	public void kill()
	{
		function =  false;
	}
	
}

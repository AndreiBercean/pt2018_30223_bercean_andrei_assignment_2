package shop;

public class Customer {
	private int cID;
	private int tWork;
	
	public Customer( int cID, int tW ){
		this.cID = cID;
		this.tWork = tW;
	}
	
	public int getCID(){
		return cID;
	}
	
	public int getWork(){
		return tWork;
	}
	
	public void process(){
		tWork -= 1;
	}
	
	public String toString(){
		return (tWork+", "+cID);
	}
}

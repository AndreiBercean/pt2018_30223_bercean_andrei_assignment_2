package shop;

import java.util.Vector;

import javax.swing.JTextArea;

public class Scheduler extends Thread 
{
	private boolean function = true;
	private int min;
	private int max;
	private int nr_POS;
	private int time_active;
	private JTextArea log;
	private JTextArea avg;
	private JTextArea top;
	private Print p;
	private Vector<POS> Checkout = new Vector<POS>();
	private Vector<Customer> Pool = new Vector<Customer>();
	
	public Scheduler(int c, int t, int mi, int ma, JTextArea s, JTextArea l, JTextArea av, JTextArea to)
	{
		avg = av;
		top = to;
		min = mi;
		max = ma - mi + 1;
		function = true;
		nr_POS = c;
		time_active = t;
		log = l;
		p = new Print(s, this);
		for(int i = 0; i<nr_POS; i++)
		{
			Checkout.addElement(new POS("POS " + i, time_active, log));
		}
	}
	
	public void info()
	{
		float sum = 0;
		int maximum = -1;
		int aux = Checkout.size() - 1;
		for(int i = 0; i <= aux; i++)
		{
			if(Checkout.elementAt(i).time() > maximum) maximum = Checkout.elementAt(i).time();
			sum += Checkout.elementAt(i).time();
		}
		sum /= aux+1;
		avg.setText(sum+"");
		float aux2 = Float.parseFloat(top.getText());
		if(aux2<maximum)top.setText(maximum+"");
	}
	
	public void kill()
	{
		function = false;
		int i = 0;
		while(i<nr_POS)
		{
			if(Checkout.elementAt(i).kill())i++;
		}
		p.kill();
	}
	
	public void run()
	{
		int eleCounter = 0;
		p.start();
		for(int i = 0; i < nr_POS; i++)
		{
			Checkout.elementAt(i).start();
		}
		while(function)
		{
			for(int j = 0; j <= (int)(Math.random() * 5); j++)
			{
				Pool.addElement(new Customer(++eleCounter,(int)((Math.random()*max)+min)));
			}
			while(Pool.size()>0)
			{
				int minimum = Checkout.elementAt(Checkout.size()-1).time();
				int aux = Checkout.size() - 1;
				for(int i = 0; i <= aux; i++)
				{
					if(Checkout.elementAt(i).time() <= minimum) minimum = i;
				}
				Checkout.elementAt(minimum).addCustomer(Pool.elementAt(0));
				Pool.removeElementAt(0);
			}
			try {
				Thread.sleep((long) (Math.random()*3000));
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String toString()
	{
		String s = new String("");
		for(int i = 0; i<nr_POS; i++)
		{
			s = s + Checkout.elementAt(i).toString();
		}
		return s;
	}
}

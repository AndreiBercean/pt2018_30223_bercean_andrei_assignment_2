package shop;

import java.util.*; 
import javax.swing.JTextArea;

public class POS extends Thread
{
	private boolean function = true;
	private Vector<Customer> queue = new Vector<Customer>();
	private int counter = 0;
	private int limit;
	JTextArea log;
	
    static Timer timer = new Timer("ThreadTimer");
	TimerTask timerTask = new TimerTask()
	{
        @Override
        public void run() 
        {
        	counter += 1;
        }
    };
    
	public POS( String name, int lim, JTextArea l )
	{
		function = true;
		setName( name );
		log = l;
		limit = lim;
		counter = 0;
		timer.scheduleAtFixedRate(timerTask, 0, 1000);
	}
	
	public void run()
	{
		while(function)
		{
			try
			{
				if(queue.size()>0)processCustomer();
				Thread.sleep(1000);
			}
			catch(InterruptedException e)
			{
				System.out.println("ERROR: " + e.toString());
			}
		}
	}
	
	public synchronized int size()
	{
		return queue.size();
	}
	
	public synchronized int time()
	{
		int wait = 0;
		for(int i = 0; i < queue.size(); i++)
		{
			wait += queue.elementAt(i).getWork();
		}
		return wait;
	}
	
	public synchronized void addCustomer( Customer c )
	{
		if(counter<= limit)queue.addElement(c);
	}
	
	private synchronized void processCustomer() throws InterruptedException
	{
		Customer c = (Customer)queue.elementAt(0);
		c.process();
		if(c.getWork() <= 0)
		{
			queue.removeElementAt(0);
			log.append("Clientul "+c.getCID()+" a fost deservit de "+getName()+" la momentul "+counter+"\n");
		}
	}
	
	public boolean kill()
	{
		function = false;
		return true;
	}
	
	public String toString()
	{
		String s = new String(getName()+": ");
		for(int i = 0; i < queue.size(); i++)
		{
			s = s + "(" + queue.elementAt(i).toString() + ") ";
		}
		return s + "\n";
	}
}
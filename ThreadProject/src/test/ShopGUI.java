package test;
import shop.*;

import java.awt.Color;
import java.awt.event.*;

import javax.swing.*;

public class ShopGUI 
{
	static Scheduler s;
	public static void main(String[] args)
	{
		Color MAROON = new Color(128, 0, 0);
		JFrame sim = new JFrame("Simulation");
		JPanel panel = new JPanel();
		
		sim.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sim.setSize(700,800);
		panel.setLayout(null);
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setForeground(Color.BLACK);
		
		JLabel sim_label = new JLabel("POS");
		sim_label.setBounds(10, 10, 470, 20);
		panel.add(sim_label);
		JTextArea sim_Box  = new JTextArea();
		sim_Box.setBounds(10,30,550,200);
		sim_Box.setBackground(Color.WHITE);
		sim_Box.setForeground(Color.BLACK);
		sim_Box.setEditable(false);
        sim_Box.setLineWrap(true);
		panel.add(sim_Box);
		
		JLabel log_label = new JLabel("Event log");
		log_label.setBounds(10, 240, 470, 20);
		panel.add(log_label);
		JTextArea log_Box = new JTextArea();
		log_Box.setBounds(10,260,550,600);
		log_Box.setBackground(Color.WHITE);
		log_Box.setForeground(Color.BLACK);
		log_Box.setEditable(false);
        log_Box.setLineWrap(true);
		panel.add(log_Box);
		
		JLabel avg_label = new JLabel("Average wait time");
		avg_label.setBounds(570, 10, 470, 20);
		panel.add(avg_label);
		JTextArea avg_Box = new JTextArea();
		avg_Box.setBounds(570,30,100,35);
		avg_Box.setBackground(Color.WHITE);
		avg_Box.setForeground(Color.BLACK);
		avg_Box.setEditable(false);
		avg_Box.setLineWrap(true);
		panel.add(avg_Box);
        
        JLabel top_label = new JLabel("Peak wait time");
		top_label.setBounds(570, 60, 470, 20);
		panel.add(top_label);
		JTextArea top_Box = new JTextArea("0");
		top_Box.setBounds(570,80,100,35);
		top_Box.setBackground(Color.WHITE);
		top_Box.setForeground(Color.BLACK);
		top_Box.setEditable(false);
		top_Box.setLineWrap(true);
		panel.add(top_Box);
		
		sim.setResizable(false);
		sim.add(panel);
		sim.setVisible(true);
		
///////////////////////////////////////////////////////////////
		
		JFrame set = new JFrame("Settings");
		JPanel panel_set = new JPanel();
		
		set.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		set.setSize(250,300);
		panel_set.setLayout(null);
		panel_set.setBackground(Color.LIGHT_GRAY);
		panel_set.setForeground(Color.BLACK);
		
		JLabel POS_label = new JLabel("Number of POS");
		POS_label.setBounds(130, 10, 470, 20);
		panel_set.add(POS_label);
		JTextField POS_Box = new JTextField();
		POS_Box.setBounds(130,30,100,35);
		POS_Box.setBackground(Color.WHITE);
		POS_Box.setForeground(Color.BLACK);
		panel_set.add(POS_Box);
		
		JLabel time_label = new JLabel("Simulation timer");
		time_label.setBounds(130, 80, 470, 20);
		panel_set.add(time_label);
		JTextField time_Box = new JTextField();
		time_Box.setBounds(130,100,100,35);
		time_Box.setBackground(Color.WHITE);
		time_Box.setForeground(Color.BLACK);
		panel_set.add(time_Box);
		
		JLabel min_label = new JLabel("Min processing time");
		min_label.setBounds(10, 10, 470, 20);
		panel_set.add(min_label);
		JTextField min_Box = new JTextField();
		min_Box.setBounds(10,30,100,35);
		min_Box.setBackground(Color.WHITE);
		min_Box.setForeground(Color.BLACK);
		panel_set.add(min_Box);
		
		JLabel max_label = new JLabel("Max processing time");
		max_label.setBounds(10, 80, 470, 20);
		panel_set.add(max_label);
		JTextField max_Box = new JTextField();
		max_Box.setBounds(10,100,100,35);
		max_Box.setBackground(Color.WHITE);
		max_Box.setForeground(Color.BLACK);
		panel_set.add(max_Box);

		JButton start_button = new JButton("Start");
		start_button.setBounds(65,160,100,35);
		start_button.setBackground(MAROON);
		start_button.setForeground(Color.WHITE);
		start_button.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				POS_Box.setEditable(false);
				time_Box.setEditable(false);
				min_Box.setEditable(false);
				max_Box.setEditable(false);
				start_button.setEnabled(false);
				
				log_Box.setText("");
				top_Box.setText("0");
				
				int pos = Integer.parseInt(POS_Box.getText());
				int time = Integer.parseInt(time_Box.getText());
				int min = Integer.parseInt(min_Box.getText());
				int max = Integer.parseInt(max_Box.getText());
				
				s = new Scheduler(pos, time, min, max, sim_Box, log_Box, avg_Box, top_Box);
				s.start();
			}
		});
		panel_set.add(start_button);
		
		JButton reset_button = new JButton("Stop");
		reset_button.setBounds(65,200,100,35);
		reset_button.setBackground(MAROON);
		reset_button.setForeground(Color.WHITE);
		reset_button.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				POS_Box.setEditable(true);
				time_Box.setEditable(true);
				min_Box.setEditable(true);
				max_Box.setEditable(true);
				start_button.setEnabled(true);
				s.kill();
			}
		});
		panel_set.add(reset_button);
		
		set.setResizable(false);
		set.add(panel_set);
		set.setVisible(true);
	}
}
